#!/bin/bash

set -e

# pushd is the same as cd but stays there.
# go to Service Directory

# add dependencies to local repo
pushd utilities 
mvn clean install
popd

